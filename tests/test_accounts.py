# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Fanalytical Inc..

"""Test Accounts."""

import json
import os

import pytest
from helpers import ordered
from jsonschema import ValidationError, validate

from translations import accounts


@pytest.mark.parametrize(
    "test_input,expected",
    [
        ({}, {}),  # External Ids
        ({'Full_Account_ID__c': None}, {}),
        ({'Banner_ID__c': None}, {}),
        ({'Patron_ID__c': None}, {}),
        (
            {'Patron_ID__c': '55851'},
            {'external_ids': ['paciolan:55851']},
        ),
        (
            {'Banner_ID__c': '01941463'},
            {'external_ids': ['banner:01941463']},
        ),
        (
            {
                'Full_Account_ID__c': '0011I00000jhWCaQAM',
                'Patron_ID__c': '55851',
                'Banner_ID__c': '01941463',
            },
            {
                'external_ids': [
                    'salesforce:0011I00000jhWCaQAM',
                    'paciolan:55851',
                    'banner:01941463',
                ]
            },
        ),
        # Owner
        ({'OwnerId': None}, {}),
        (
            {'OwnerId': '0011I00000jhWCaQAM'},
            {'owner_id': 'salesforce:0011I00000jhWCaQAM'},
        ),
        # Name
        ({'LastName': None, 'FirstName': None}, {}),
        (
            {'LastName': 'Last', 'FirstName': 'First'},
            {'last_name': 'Last', 'first_name': 'First'},
        ),
        # Birthday
        ({'PersonBirthdate': None}, {}),
        ({'PersonBirthdate': '1990-01-01'}, {'birthdate': '1990-01-01'}),
        (
            {'PersonBirthdate': '1990 Jan 1st'},
            {},
        ),
        # Phone
        ({'Phone': '(999)-999-9999'}, {}),
        ({'Phone': '(000)-000-0000'}, {}),
        ({'Phone': 'abc-asd-asdd'}, {}),
        (
            {'Phone': '(999)-999-8888', 'PersonHomePhone': '999/999-8888'},
            {'phone': ['9999998888']},
        ),
        (
            {
                'Phone': '(999)-999-8888',
                'PersonAssistantPhone': '987-654-1234',
            },
            {'phone': ['9999998888', '9876541234']},
        ),
        # Email
        ({'PersonEmail': 'test@test.com'}, {'email': ['test@test.com']}),
        (
            {
                'PersonEmail': 'test@test.com',
                'Secondary_Email__pc': 'second@second.com',
            },
            {'email': ['test@test.com', 'second@second.com']},
        ),
        (
            {
                'Business_Email__c': 'business@business.com',
                'PersonEmail': 'test@test.com',
            },
            {'email': ['test@test.com', 'business@business.com']},
        ),
        # Address
        (
            {'BillingAddress': {'city': 'New York', 'postalCode': '10036'}},
            {
                'address': [
                    {
                        'city': 'New York',
                        'postal_code': '10036',
                        'country': 'US',
                    }
                ]
            },
        ),
        (
            {
                'BillingAddress': {
                    'city': 'Toronto',
                    'postalCode': 'M1R 0E9',
                    'country': 'CA',
                }
            },
            {
                'address': [
                    {
                        'city': 'Toronto',
                        'postal_code': 'M1R 0E9',
                        'country': 'CA',
                    }
                ]
            },
        ),
        (
            {
                'BillingAddress': {'city': 'New York', 'postalCode': '10036'},
                'PersonMailingAddress': None,
                'BillingCity': 'Different',
            },
            {
                'address': [
                    {
                        'city': 'New York',
                        'postal_code': '10036',
                        'country': 'US',
                    }
                ],
            },
        ),
        (
            {
                'PersonMailingState': 'SC',
                'PersonMailingAddress': {'state': 'NC', 'postalCode': '27514'},
            },
            {
                'address': [
                    {'state': 'NC', 'postal_code': '27514', 'country': 'US'}
                ],
            },
        ),
        (
            {
                'PersonOtherCountry': 'SP',
                'PersonOtherStreet': 'street',
                'PersonOtherPostalCode': '12345',
                'PersonMailingAddress': {'state': 'NC', 'postalCode': '27514'},
            },
            {
                'address': [
                    {'state': 'NC', 'postal_code': '27514', 'country': 'US'},
                    {
                        'country': 'SP',
                        'street': 'Street',
                        'postal_code': '12345',
                    },
                ]
            },
        ),
        # Letter winner
        ({'Men_s_Basketball_Full__c': False}, {}),
        (
            {'Men_s_Basketball_Full__c': True},
            {
                'letterwinner': ['MBB'],
                'type': ['alumni'],
                'degree': [{'has_scholarship': True}],
            },
        ),
        (
            {'Men_s_Basketball_Full__c': True, 'Football_Full__c': True},
            {
                'letterwinner': ['MBB', 'FB'],
                'type': ['alumni'],
                'degree': [{'has_scholarship': True}],
            },
        ),
        (
            {
                'Men_s_Basketball_Full__c': True,
                'Football_Full__c': True,
                'Women_s_Basketball_Full__c': True,
            },
            {
                'letterwinner': ['MBB', 'FB', 'WBB'],
                'type': ['alumni'],
                'degree': [{'has_scholarship': True}],
            },
        ),
        (
            {
                'Men_s_Basketball_Full__c': True,
                'Football_Full__c': True,
                'Women_s_Basketball_Full__c': True,
                'Lacrosse_Full__c': True,
            },
            {
                'letterwinner': ['MBB', 'FB', 'WBB', 'ML'],
                'type': ['alumni'],
                'degree': [{'has_scholarship': True}],
            },
        ),
        # Type
        ({'School_Alum__pc': False}, {}),
        ({'School_Alum__pc': True}, {'type': ['alumni']}),
        ({'Customer_Type__c': 'YA'}, {'type': ['young alumni', 'alumni']}),
        (
            {'Customer_Type__c': 'YA', 'School_Alum__pc': True},
            {'type': ['young alumni', 'alumni']},
        ),
        (
            {'Customer_Type__c': 'YA', 'School_Alum__pc': False},
            {'type': ['young alumni', 'alumni']},
        ),
        # Degree
        ({'School_Grad_Year__pc': None}, {}),
        (
            {'School_Grad_Year__pc': '1990'},
            {'degree': [{'year': '1990'}], 'type': ['alumni']},
        ),
        (
            {'School_Grad_Year__pc': '1990-01-10'},
            {'degree': [{'year': '1990'}], 'type': ['alumni']},
        ),
        (
            {'School_Grad_Year__pc': '1990 Jan 10'},
            {'degree': [{'year': '1990'}], 'type': ['alumni']},
        ),
        (
            {'School_Grad_Year__pc': 'abc'},
            {'type': ['alumni']},
        ),
        # Salary
        ({'AnnualRevenue': None}, {}),
        ({'AnnualRevenue': '10000'}, {'salary': 10000}),
        ({'AnnualRevenue': 10000}, {'salary': 10000}),
        ({'AnnualRevenue': '10K'}, {}),
        # Company
        ({'Employer__c': None}, {}),
        ({'Employer__c': 'Name'}, {'company': 'Name'}),
        # Industry
        ({'Industry': None}, {}),
        ({'Industry': 'Name'}, {'industry': 'Name'}),
        # Number Of Employees
        ({'NumberOfEmployees': None}, {}),
        (
            {'NumberOfEmployees': 100},
            {'number_of_employees': 100, 'type': ['company']},
        ),
        (
            {'NumberOfEmployees': '100'},
            {'number_of_employees': 100, 'type': ['company']},
        ),
        ({'NumberOfEmployees': '1k'}, {'type': ['company']}),
        # Booster level
        ({'Nova_Points__c': 123}, {'booster_level': '123'}),
        # Flags
        ({'IsPersonAccount': True}, {}),
        ({'IsPersonAccount': None}, {'type': ['company']}),
        ({'Inactive__pc': True}, {'is_active': False}),
        ({'Inactive__pc': None}, {'is_active': True}),
        ({'PersonDoNotCall': True}, {'do_not_call': True}),
        ({'PersonDoNotCall': None}, {'do_not_call': False}),
        ({'Adobe_Opt_Out__pc': True}, {'do_not_email': True}),
        ({'Adobe_Opt_Out__pc': 'Yes'}, {'do_not_email': True}),
        ({'Adobe_Opt_Out__pc': None}, {'do_not_email': False}),
        ({'Adobe_Opt_Out__pc': False}, {'do_not_email': False}),
        ({'Account_Flag__c': True}, {'type': ['elite']}),
        ({'Account_Flag__c': None}, {}),
    ],
)
def test_fields(test_input, expected):
    """General simple field test.

    The first element of the list is the dictionary we receive from the source,
    i.e. ``{'Full_Account_ID__c': '0011U00000Nfvg9QAB'}`` and the second is the
    output from the translation i.e.
    ``{'external_ids': [{'origin': 'sf', 'id': '0011U00000Nfvg9QAB'}]}``.
    """
    # Add default fields to the expected value
    res = accounts.translate(test_input)
    if res:
        # Remove field added by post-process
        assert res.pop('scores', None) is not None
        assert res.pop('organization_id') == 'test-uuid'
    assert ordered(res) == ordered(expected)


def test_account_valid_info():
    """Test schema validation."""
    pwd = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(pwd, 'accounts.json')) as f:
        schema = json.load(f)

    with pytest.raises(ValidationError):
        validate({}, schema)

    raw = {
        'Patron_ID__c': '55851',
        'Banner_ID__c': '01941463',
        'Full_Account_ID__c': '0011I00000jhWCaQAM',
        'OwnerId': '0011I00000jhWCaQAM',
        'LastName': 'Last',
        'FirstName': 'First',
        'PersonBirthdate': '1990-01-01',
        'Phone': '(999)-999-8888',
        'PersonHomePhone': '999/999-8888',
        'PersonEmail': 'test@test.com',
        'BillingAddress': {'city': 'New York', 'postalCode': '10036'},
        'PersonOtherCountry': 'SP',
        'PersonOtherStreet': 'street',
        'PersonOtherPostalCode': '12345',
        'PersonMailingAddress': {'state': 'NC', 'postalCode': '27514'},
        'Men_s_Basketball_Full__c': True,
        'School_Alum__pc': True,
        'Customer_Type__c': 'YA',
        'School_Grad_Year__pc': '1990-01-10',
        'AnnualRevenue': '10000',
        'NumberOfEmployees': 100,
        'Nova_Points__c': 123,
        'Inactive__pc': None,
        'PersonDoNotCall': None,
        'Adobe_Opt_Out__pc': False,
        'Account_Flag__c': None,
    }

    account = accounts.translate(raw)

    validate(account, schema)
